function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf('http://sandbox.lavbic.net/teaching/OIS/gradivo/') > -1;
  if (jeSmesko) {
    sporocilo = sporocilo.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace('&lt;img', '<img').replace('png\' /&gt;', 'png\' />');
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  } else {
    return $('<div style="font-weight: bold;"></div>').text(sporocilo);
  }
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

//Klice se, ko user vnese sporocilo
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;

//Ce se zacne z /, gre za ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo=filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

//Povezava na socket
  $('#poslji-sporocilo').val('');
}

var trenutniVzdevek="", trenutniKanal="";
var socket = io.connect();
var vulgarneBesede=[];

//Uporaba lib jQuery - zato oneliner
$.get("/swearWords.txt", function(podatki){
  vulgarneBesede=podatki.split("\n");
})

function filtrirajVulgarneBesede(vhod){
  //vhod=sporocilo;
  
  for(var i=0; i<vulgarneBesede.length; i++){
    //Regularni izrazi - \\b izključi podnize, g je global, i je case insensitive
    vhod=vhod.replace(new RegExp("\\b"+vulgarneBesede[i] + "\\b", "gi"), function(){
      var zamenjava ="";
      for(var j=0; j<vulgarneBesede[i].length; j++){
        zamenjava=zamenjava+"*";
      }
      return zamenjava;
    }); 
  }
  return vhod;
}

//Poslušamo na socketu - vzdevek, sprememba, odgovor
$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek=rezultat.vzdevek;
      $("#kanal").text(trenutniVzdevek+" @ "+trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    
    //Vse funkcionalnisti v zvezi s spremembo kanala
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  //Cakamo na sprocila
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal=rezultat.kanal;
    $("#kanal").text(trenutniVzdevek+" @ "+trenutniKanal);
    // Brisano, zamenjano z zgornjo $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  //Funkcionalnost seznama kanalov - od serverja dobimo seznam, ga samo izpisemo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  //Posljemo strezniku (kot user) podatek, na katerem kanalu smo
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


//Dodajanje smeskov
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.replace(smesko,
      "<img src='http://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}
